#Scrim Bot - mmatt

import discord
import asyncio
import logging
import json

from discord.ext import commands

#def get_prefix(client,message):
    #with open('prefix.json', 'r') as f:
        #prefix = json.load(f)

    #return prefix[str(message.guild.id)]

client = commands.Bot(command_prefix = '-')

#@client.event
#async def on_guild_join(guild):
    #with open('prefix.json', 'r') as f:
        #prefix = json.load(f)

    #prefix[str(guild.id)] = '-'

    #with open('prefix.json', 'w') as f:
        #json.dump(prefix, f, indent=4)

#@client.event
#async def on_guild_remove(guild):
    #with open('prefix.json', 'r') as f:
        #prefix = json.load(f)

    #prefix.pop(str(guild.id))

    #with open('prefix.json', 'w') as f:
        #json.dump(prefix, f, indent=4)    

@client.event
async def on_ready():
    print ("Scrim Bot has ready upped")

@client.command()
@commands.has_permissions(manage_messages=True)
async def solos(ctx):
    embed = discord.Embed(title="An admin has used the Solo scrim command.", description="Solo scrims starting in about 5-10 minutes.")
    embed.add_field(name="Solo scrims starting", value="soon, join the `Countdown Channel` to play.")
    embed.set_footer(text = "Enter the last 3 digits of your match ID in the `#last-3` channel")

    await ctx.send(embed=embed)


@client.command(aliases=['information'])
async def info(ctx):
    embed = discord.Embed(title="Dizzy Scrim Bot Information", color = 0x00ff00)
    embed.add_field(name="Name", value="Dizzy Scrim Bot")
    embed.add_field(name="Description", value="A Discord bot made my mmatt#0001 using discord.py. This bot was made by a beginner so that's why it is bad xd.")
    embed.add_field(name="Command Prefix", value="-")
    embed.add_field(name="Hosted on", value="AWS Serv")
    embed.add_field(name="Ping", value=":ping_pong: {0}".format(round(client.latency, 1)))
    embed.add_field(name="Library", value="discord.py")
    embed.add_field(name="Invite Link", value="https://discordapp.com/api/oauth2/authorize?client_id=CLIENT_ID_HERE&scope=bot&permissions=0")
    embed.add_field(name="Github Repo", value="https://gitlab.com/mmatt625/dzzy-Scrim-Bot")
    embed.set_thumbnail(url="https://static-cdn.jtvnw.net/jtv_user_pictures/e114ab54-6914-47d1-b44d-42a78b64fa2f-profile_image-300x300.png")
    embed.set_footer(text="Thank you for using the Dizzy scrim bot!")

    await ctx.send(embed=embed)

@client.command()
@commands.has_permissions(manage_messages=True)
async def duos(ctx):
    embed = discord.Embed(title="An admin has used the Duo scrim command.", description="Duo scrims starting in about 5-10 minutes.")
    embed.add_field(name="Duo scrims starting", value="soon, only `1 PARTNER` join the `Countdown Channel` to play.")
    embed.set_footer(text = "1 PERSON FROM YOUR GROUP!! Enter the last 3 digits of your match ID in the `#last-3` channel")

    await ctx.send(embed=embed)

@client.command()
@commands.has_permissions(manage_messages=True)
async def squads(ctx):
    embed = discord.Embed(title="An admin has used the Squad scrim command.", description="Squad scrims starting in about 5-10 minutes.")
    embed.add_field(name="Squad scrims starting", value="soon, only `1 PERSON FROM YOUR GROUP!! ` join the `Countdown Channel` to play.")
    embed.set_footer(text = "1 PERSON FROM YOUR GROUP!! Enter the last 3 digits of your match ID in the `#last-3` channel")

    await ctx.send(embed=embed)

@client.command()
@commands.has_permissions(manage_messages=True)
async def end(ctx):
    embed = discord.Embed(title="An admin has used the end command.", description="Scrims over.")
    embed.add_field(name="Thank you for playing!", value="Thank you for playing in the LaZy Scrims using the LaZy Scrim bot, we would like to thank anyone that came out into the scrim!")
    embed.set_footer(text="<3")

    await ctx.send(embed=embed)

@client.command()
async def support(ctx):
    embed = discord.Embed(title="Need Support?", description="The Discord Support Server and the dev's contact.", colour=discord.Color.blue(), url="https://discord.gg/8xMWb7W")

    embed.add_field(name="Contact the Dev", value="<@308000668181069824>")
    embed.add_field(name="Join the Discord", value="https://discord.gg/8xMWb7W")

    await ctx.send(embed=embed)


#@client.command(aliases=['changeprefix'])
#@commands.has_permissions(administrator=True)
#async def prefix(ctx,prefix):
   #with open('prefix.json', 'r') as f:
        #prefix = json.load(f)

    #prefix[str(ctx.guild.id)] = prefix

    #with open('prefix.json', 'w') as f:
        #json.dump(prefix, f, indent=4)

client.run("TOKEN")
#hi mr jones lol